/**
 * @file
 * Used in admin settings form.
 */

(function ($) {
  'use strict';

  /**
   * Empty select easily in admin settings form.
   */
  Drupal.behaviors.userAccessTimeslotEmptySelect = {
    attach: function (context, settings) {
      $('.user-access-timeslot-empty-form').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).parent('.fieldset-wrapper').find('select').val('');
      });
    }
  };

})(jQuery);
