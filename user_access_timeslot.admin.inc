<?php

/**
 * @file
 * Used to define module settings.
 */

/**
 * Settings form callback.
 */
function user_access_timeslot_settings_form($form, &$form_state) {
  $roles = user_roles();

  $form['#prefix'] = '<div id="user-access-timeslot-settings-form-wrapper">';
  $form['#suffix'] = '</div>';

  $zones = system_time_zones();
  $zone = drupal_get_user_timezone();

  $timezone[] = l(t('Timezone used for control : @timezone', array(
    '@timezone' => $zones[$zone],
  )), 'admin/config/regional/settings');

  if (variable_get('configurable_timezones', 1)) {
    $timezone[] = t('<strong>Be careful</strong>, user timezone are activated');
    $timezone[] = t('Controls will depend on each user timezone');
  }

  $form['timezone'] = array(
    '#markup' => implode('<br />', $timezone),
  );

  $options = array();
  foreach ($roles as $rid => $role) {
    if ($rid != 1) {
      $options[$rid] = $role;
    }
  }

  if (!empty($options)) {
    $help[] = t('Choose the role for which you want to configure access.');
    $help[] = t('Control is inactive until you choose a value in all selects.');
    $help[] = t('All user can access as they want inactive slots.');
    $help[] = t('For multiple roles, only one positive result is needed.');
    $help[] = t("Changes don't impact users already logged.");

    $form['role'] = array(
      '#title' => t('Role'),
      '#description' => implode('<br />', $help),
      '#type' => 'select',
      '#options' => $options,
      '#required' => TRUE,
      '#ajax' => array(
        'callback' => 'user_access_timeslot_settings_form_role_submit',
        'wrapper' => 'user-access-timeslot-settings-form-wrapper',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );

    if (!empty($form_state['values']['role'])) {
      $role = $form_state['values']['role'];

      $days = _user_access_timeslot_get_days();

      $hours['am'] = array(
        '' => t('hh'),
        '00' => '00',
      );
      $hours['pm'] = array(
        '' => t('hh'),
        '12' => '12',
      );
      for ($i = 1; $i < 12; $i++) {
        if ($i < 10) {
          $i = '0' . $i;
        }

        $hours['am'][$i] = $i;
        $hours['pm'][$i + 12] = $i;
      }

      $minutes = array('' => t('mm'));
      for ($i = 0; $i < 60; $i++) {
        if ($i < 10) {
          $i = '0' . $i;
        }
        $minutes[$i] = $i;
      }

      $suffix = l(t('Click here to empty select list'), '<front>', array(
        'attributes' => array(
          'class' => array(
            'user-access-timeslot-empty-form',
          ),
        ),
      ));

      $module_path = drupal_get_path('module', 'user_access_timeslot');
      $js_path = $module_path . '/js/user_access_timeslot.js';
      $form['#attached']['js'][] = $js_path;

      foreach ($days as $k => $day) {
        $base = 'user_access_timeslot_' . $k . '_' . $role;

        $form[$k] = array(
          '#type' => 'fieldset',
          '#title' => t('@day (inactive)', array('@day' => $day)),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        );

        $types = _user_access_timeslot_get_day_slots();
        foreach ($types as $key_type => $type) {
          $form[$k][$key_type] = array(
            '#type' => 'fieldset',
            '#title' => t('@type (inactive)', array('@type' => $type)),
            '#collapsible' => FALSE,
            '#collapsed' => FALSE,
            '#attributes' => array(
              'class' => array('container-inline'),
            ),
          );

          $base_type = $base . '_' . $key_type;

          if (variable_get($base_type . '_h_from', FALSE)
            && variable_get($base_type . '_m_from', FALSE)
            && variable_get($base_type . '_h_to', FALSE)
            && variable_get($base_type . '_m_to', FALSE)) {
            $form[$k][$key_type]['#title'] = t('@type (active)', array(
              '@type' => $type,
            ));
            $form[$k]['#title'] = $day;
            $form[$k]['#collapsed'] = FALSE;
          }

          $form[$k][$key_type][$base_type . '_h_from'] = array(
            '#type' => 'select',
            '#title' => t('Authorize access between'),
            '#options' => $hours[$key_type],
            '#default_value' => variable_get($base_type . '_h_from', ''),
          );

          $form[$k][$key_type][$base_type . '_m_from'] = array(
            '#type' => 'select',
            '#title' => ' : ',
            '#options' => $minutes,
            '#default_value' => variable_get($base_type . '_m_from', ''),
          );

          $form[$k][$key_type][$base_type . '_h_to'] = array(
            '#type' => 'select',
            '#title' => t('and'),
            '#options' => $hours[$key_type],
            '#default_value' => variable_get($base_type . '_h_to', ''),
          );

          $form[$k][$key_type][$base_type . '_m_to'] = array(
            '#type' => 'select',
            '#title' => ' : ',
            '#options' => $minutes,
            '#default_value' => variable_get($base_type . '_m_to', ''),
            '#suffix' => $suffix,
          );
        }
      }
    }
  }

  // Don't use system_settings_form() directly, use ajax instead.
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
      '#ajax' => array(
        'callback' => 'user_access_timeslot_settings_form_global_submit',
        'wrapper' => 'user-access-timeslot-settings-form-wrapper',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    ),
  );

  return $form;
}

/**
 * Custom validation callback for user_access_timeslot_settings_form.
 */
function user_access_timeslot_settings_form_validate($form, &$form_state) {
  $role = $form_state['values']['role'];
  $days = _user_access_timeslot_get_days();
  $types = _user_access_timeslot_get_day_slots();
  foreach ($types as $key_type => $type) {
    foreach ($days as $k => $v) {
      $base = 'user_access_timeslot_' . $k . '_' . $role . '_' . $key_type;
      $hour_from = $form_state['values'][$base . '_h_from'];
      $min_from = $form_state['values'][$base . '_m_from'];
      $hour_to = $form_state['values'][$base . '_h_to'];
      $min_to = $form_state['values'][$base . '_m_to'];

      if ($hour_from && $min_from && $hour_to && $min_to) {
        $start = mktime($hour_from, $min_from);
        $end = mktime($hour_to, $min_to);

        // Prevent user to submit inverted dates.
        if ($end < $start) {
          $error = t("@day @type start date can't be after end date.", array(
            '@day' => $v,
            '@type' => $type,
          ));
          form_set_error('', $error);
        }
        // Prevent user to submit identical dates.
        elseif ($start == $end) {
          $error = t("@day @type start and end date can't be identical.", array(
            '@day' => $v,
            '@type' => $type,
          ));
          form_set_error('', $error);
        }
      }
    }
  }
}

/**
 * Ajax callback to update form when role is changed.
 */
function user_access_timeslot_settings_form_role_submit($form, &$form_state) {
  return $form;
}

/**
 * Ajax callback to render form with drupal messages after submission.
 */
function user_access_timeslot_settings_form_global_submit($form, &$form_state) {
  return $form;
}

/**
 * Ajax callback to save settings when form is submitted.
 */
function user_access_timeslot_settings_form_submit($form, &$form_state) {
  drupal_validate_form($form['#id'], $form, $form_state);

  if (!form_get_errors()) {
    system_settings_form_submit($form, $form_state);
  }

  $form_state['rebuild'] = TRUE;
}
