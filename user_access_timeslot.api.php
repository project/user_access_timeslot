<?php

/**
 * @file
 * Hooks provided by the User Access Timeslot module.
 */

/**
 * Allows modules to alter list of days.
 */
function hook_user_access_timeslot_get_days_alter(&$days) {
  // Remove sunday from the list.
  if (!empty($days['sun'])) {
    unset($days['sun']);
  }
}

/**
 * Allows modules to alter calculated expire date for a specific $rid.
 */
function hook_user_access_timeslot_get_expiration_date_alter($rid, $account, &$expire) {
  // Allow super heroes to never have an expiration date.
  if (!empty($account->field_is_a_super_hero[LANGUAGE_NONE][0]['value'])) {
    $expire = FALSE;
  }
}

/**
 * Allows modules to alter calculated expire dates globally.
 */
function hook_user_access_timeslot_user_login_alter($account, &$expires) {
  // Allow super heroes to never have an expiration date.
  if (!empty($account->field_is_a_super_hero[LANGUAGE_NONE][0]['value'])) {
    $expires = array(FALSE);
  }
}
