CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module allows administrators to limit users access on specific timeslots.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Go to admin/config/people/user-access-timeslot

MAINTAINERS
-----------
Current maintainers:
 * Léo Prada (Nixou) - https://drupal.org/user/2304734
